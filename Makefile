all: cwl_progress

CFLAGS=-Wall -std=c99 -pedantic -O2
LDLIBS=-lcurl -lz -lpthread

OBJECTS=cwl_progress.o sds.o cJSON.o
cwl_progress: $(OBJECTS)
	$(CC) $(CFLAGS) -o cwl_progress $(OBJECTS)  $(LDLIBS)

clean:
	$(RM) cwl_progress *.o
